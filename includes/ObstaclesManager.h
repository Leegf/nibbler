/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ObstaclesManager.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 16:22:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/08/24 17:39:49 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NIBBLER_OBSTACLESMANAGER_H
#define NIBBLER_OBSTACLESMANAGER_H

#include <vector>
#include "FoodManager.h"
#include "Snake.h"

class ObstaclesManager {
public:
	ObstaclesManager();
	~ObstaclesManager();
	void setObjectsToCheckCollisionWith(const Snake *snake,
									   const FoodManager *foodManager);
	void setObjectsToCheckCollisionWith(const Snake *snake, const Snake *snake2,
										const FoodManager *foodManager);
	void setEngine(IEngine * engine);
	void draw();
	void reset();

	ObstaclesManager &operator=(ObstaclesManager const &rhs) = delete;
	ObstaclesManager(ObstaclesManager const &src) = delete;
	void setWinAndHeight(int winWidth, int winHeight);
	void spawnRandomlyObstacles(int numOfObstacles);
	std::vector<MapObject> obstacles;

private:
	bool _checkCollision(const MapObject & obstacle) const;
	const Snake * _snake = nullptr;
	const Snake * _snake2 = nullptr;
	const FoodManager * _foodManager = nullptr;
	int _winWidth = 0;
	int _winHeight = 0;
	IEngine * _engine;
};

#endif //NIBBLER_OBSTACLESMANAGER_H
