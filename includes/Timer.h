/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Timer.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:33:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/07 15:54:16 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NIBBLER_TIMER_H
#define NIBBLER_TIMER_H

#include <chrono>
#include <functional>
#include <mutex>

class Timer {
public:
	Timer();
	~Timer() = default;
	Timer(Timer const & src) = delete;
	Timer &operator=(Timer const & rhs) = delete;
	void add(std::chrono::seconds delay,
			 std::function<void ()> callback,
			 bool isLooping = false);
private:
	bool _isRunning;
	bool _isLooping;
	std::mutex _mChanging;
	std::chrono::seconds _delay;
	std::chrono::high_resolution_clock::time_point _start;
	std::function<void()> _callback;
};
#endif //NIBBLER_TIMER_H
