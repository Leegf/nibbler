#ifndef NIBBLER_IENGINE_H
#define NIBBLER_IENGINE_H

#include "Config.h"

class IEngine {

public:
	virtual void createWindow(int width, int height) = 0;
	virtual void clearWindow() = 0;
	virtual void drawSprite(eObject objectType, eRotate rotate, int x, int y) = 0;
	virtual void drawBackground(eColor color) = 0;
	virtual void drawText(eColor color, eFont font, int x, int y, int size, std::string text) = 0;
	virtual void drawRectangle(eColor color, int x, int y, int width, int height) = 0;
	virtual void updateWindow() = 0;
	virtual eKey keyBoardEvent() = 0;
    virtual bool keyBoardEvent(eKey key) = 0;
	virtual ~IEngine(){}
};

using create_func = IEngine *(*)();
using destroy_func = void (*)(IEngine *);

#endif //NIBBLER_IENGINE_H
