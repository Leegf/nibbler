#ifndef NIBBLER_ISOUNDENGINE_H
#define NIBBLER_ISOUNDENGINE_H

#include "Config.h"

class ISoundEngine {

public:
	virtual void playSound(eSound sound) = 0;
	virtual void playMusic(eMusic music) = 0;
	virtual ~ISoundEngine(){};
};

using create_sound_func = ISoundEngine *(*)();
using destroy_sound_func = void (*)(ISoundEngine *);
#endif //NIBBLER_ISOUNDENGINE_H
