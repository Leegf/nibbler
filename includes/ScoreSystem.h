/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScoreSystem.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 17:43:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/29 15:06:46 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NIBBLER_SCORESYSTEM_H
#define NIBBLER_SCORESYSTEM_H

#include "IEngine.h"

class ScoreSystem {
public:
	ScoreSystem();
	~ScoreSystem();
	ScoreSystem &operator=(ScoreSystem const &rhs) = delete;
	ScoreSystem(ScoreSystem const &src) = delete;
	void setParameters(eFont font, eColor color, int FontSize, int yPos = 0, int xPos = 0);
	void scoreBonus();
	void setPlayerName(const std::string & name);
	int getScore() const;
	void nextLvl();
	int getLvl() const;
	void scoreSuperBonus();
	void setEngine(IEngine * engine);
	void resetScore();
	void resetLvl();
	void draw();
	void setSecondPlayerMode(bool isSecondPlayer = true);

private:
	std::string _playerName;
	int _currentScore = 0;
	int _curLvl = 1;
	eFont _font;
	eColor _color;
	int _fontSize;
	int _xPos;
	int _yPos;
	IEngine * _engine;
	bool _secondPlayer = false;
};

#endif //NIBBLER_SCORESYSTEM_H
