/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FoodManager.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 14:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/08/24 16:59:43 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NIBBLER_FOODMANAGER_H
#define NIBBLER_FOODMANAGER_H

#include "MapObject.h"
#include "IEngine.h"
#include "Timer.h"

class Snake;

class FoodManager {
public:
	FoodManager();
	FoodManager(FoodManager const &src) = delete;
	~FoodManager();
	FoodManager &operator=(FoodManager const &rhs) = delete;
	void setWinAndHeight(int winWidth, int winHeight);
	void RefreshFood();
	void RefreshSuperFood();
	void setObjectsToCheckCollisionWith(const Snake * snake, const std::vector<MapObject> * obstacles);
	void setObjectsToCheckCollisionWith(const Snake * snake, const Snake * snake2, const std::vector<MapObject> * obstacles);
	void reset();
	void Start();
	void setEngine(IEngine * engine);
	void draw();
	const MapObject & getFood() const;
	const MapObject & getSuperFood() const;
private:
	void _initializeProperties();
	void _spawnRandomlyFood(MapObject * food);
	bool _checkCollision(const MapObject & food) const;
	void _removeSuperFood();
	int _winWidth = 0;
	int _winHeight = 0;
	MapObject _food;
	MapObject _superFood;
	const Snake * _snake = nullptr;
	const Snake * _snake2 = nullptr;
	const std::vector<MapObject> * _obstacles = nullptr;
	IEngine * _engine;
	Timer _timer;
	Timer _timerForRemovingSuperFood;
};

#endif //NIBBLER_FOODMANAGER_H
