#ifndef NIBBLER_LIBMANAGER_H
#define NIBBLER_LIBMANAGER_H

#include "IEngine.h"
#include "Config.h"
#include "ISoundEngine.h"

class LibManager {

public:
    LibManager() = default;
    ~LibManager() = default;
    LibManager(const LibManager &) = delete;
    LibManager &operator=(const LibManager &) = delete;

	IEngine *createEngine(eLib lib);
	ISoundEngine *createSoundEngine();
	void destroyEngine();
    void destroySoundEngine();
	IEngine *changeEngine(eLib lib);
	eLib curLib;

private:
	void *_dlHandle;
	void *_dlSoundHandle;

	IEngine *_engine;
	ISoundEngine *_soundEngine;

	create_func _createEngine;
	destroy_func _destroyEngine;

	create_sound_func _createSoundEngine;
	destroy_sound_func _destroySoundEngine;
};
#endif //NIBBLER_LIBMANAGER_H
