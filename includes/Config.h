/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Config.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/27 17:46:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/10/19 16:16:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONFIG_H
# define CONFIG_H

#include <iostream>

//for Menu
#define MENU_FONT_SIZE 20
#define MARGIN_BETWEEN_ENTRIES 0
#define TITLE_FONT_SIZE 70
#define TEXT_FONT_SIZE 20
#define TITLE_NAME "NIBBLER"

//for Scores
#define FOOD_VALUE 5
#define SUPERFOOD_VALUE 20
#define SCORE_FONT_SIZE 30

//for FoodManager
#define SUPERFOOD_SPAWN_INTERVAL 10
#define SUPERFOOD_LIFETIME 5

//Max and min sizes of window
#define MIN_WIN_WIDTH 20
#define MIN_WIN_HEIGHT 25
#define MAX_WIN_WIDTH 80
#define MAX_WIN_HEIGHT 40

//in-game
#define SPRITE_SIZE 32
#define GAMEOVER_FONT_SIZE 65
#define DEFAULT_SPEED 70
#define SPEED_STEP 5 //step change for speed when advancing to the next lvl
#define NUM_OF_OBSTACLES 10
#define OBSTACLES_STEP 5
#define LVL_UP_SCORE 100
#define WIN_LVL 6
#define MUL_WIN_SCORE 100

//sound
#define SOUNDS_NUM 8
#define MUSIC_NUM 1


enum class eObject {
	Food,
	Obstacle,
	Body,
	Head,
	BodyDownRight,
	BodyDownLeft,
	BodyUpRight,
	BodyUpLeft,
	Tail,
	SuperFood,
	Body2,
	Head2,
	BodyDownRight2,
	BodyDownLeft2,
	BodyUpRight2,
	BodyUpLeft2,
	Tail2,
	None
};

enum class eRotate {
	Up,
	Down,
	Left,
	Right
};

enum class eKey {
	None,
	Quit,
	MoveUp,
	MoveDown,
	MoveLeft,
	MoveRight,
	Pause,
	Enter,
	Sasuke,
	W,
	S,
	A,
	D,
	Sdl,
	Sfml,
	Opengl
};

enum class eLib {
	Sdl2,
	Sfml,
	Opengl
};

enum class eFont {
	Pacifico,
	Naruto
};

enum class eColor {
	Purple,
	Black,
	Blue,
	White,
	Green,
	Orange
};

enum class eSound {
	Sasuke,
	Naruto,
	Ramen,
	GameOver,
	GameWon,
	NewLvl,
	MenuChange,
	MenuSelect
};

enum class eMusic {
	Konoha
};

const std::string Music [] {
	"resources/Afternoon_of_Konoha.wav"
};

const std::string Sounds [] {
	"resources/sasuke!!!!.wav",
	"resources/naruto!!!.wav",
	"resources/newpickup.wav",
	"resources/gameover.wav",
	"resources/gamewon.wav",
	"resources/newlevel.wav",
	"resources/menuchange.wav",
	"resources/menuselect.wav"
};
const std::string Fonts [] {
	"resources/Pacifico.ttf",
	"resources/njnaruto.ttf"
};

const uint8_t RgbColors [][3] {
	{ 128, 0, 128 },
	{ 0, 0, 0 },
	{ 0, 0, 204 },
	{ 255, 255, 255 },
	{2, 99, 15},
	{163, 79, 0}
};
const std::string ObjSpritesPath [] {
	"resources/sasuke.png",
	"resources/obstacle_shuriken.png",
	"resources/snake_body_naruto.png",
	"resources/naruto_head.png",
	"resources/snake_body_down_right_naruto.png",
	"resources/snake_body_down_left_naruto.png",
	"resources/snake_body_up_right_naruto.png",
	"resources/snake_body_up_left_naruto.png",
	"resources/snake_tail_naruto.png",
	"resources/super_ramen.png",
	"resources/sakura_body.png",
	"resources/sakura_head.png",
	"resources/sakura_body_down_right.png",
	"resources/sakura_body_down_left.png",
	"resources/sakura_body_up_right.png",
	"resources/sakura_body_up_left.png",
	"resources/sakura_tail.png",
};

const std::string LibPath[] {
		"libraries/sdl_mode/libsdlmode.so",
		"libraries/sfml_mode/libsfmlmode.so",
		"libraries/opengl_mode/libopenglmode.so"
};

enum class opFlag {
	None,
	ShowingText,
	StartGame,
	StartMulGame,
	MainMenu,
	EndGame,
	GameOver,
	Pause
};

#endif
