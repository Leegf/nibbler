/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Game.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/27 17:26:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/10/19 18:03:19 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NIBBLER_GAME_H
#define NIBBLER_GAME_H

#include <vector>
#include "Config.h"
#include "IEngine.h"
#include "MapObject.h"
#include "Snake.h"
#include "ISoundEngine.h"
#include "FoodManager.h"
#include "ObstaclesManager.h"
#include "ScoreSystem.h"
#include "Menu.h"
#include "LibManager.h"

class Game {
public:
	Game();
	Game(int winWidth, int winHeight);
	void PreloadGame(int winWidth, int winHeight);
	void Tick();
	void TickForMulGame();
	Game(Game const &src) = delete;
	~Game();
	Game &operator=(Game const &rhs) = delete;

	void drawMapObjects();
	void setEngine(IEngine * eng);
	void setSoundEngine(ISoundEngine * eng);
	void setLibManager(LibManager * manager);
	void initializeMainMenu();
	void initializePauseMenu();
	void initializeSecondPlayer();
	void reset(bool WithLevel = false, bool withLvlModifications = false);

	void gameOver();
	void gameWon();
	void gameWon(bool FirstPlayer);
	void drawDifferentBgForEngines();
	void run();
	bool checkScoreAndRunNextLvl();
	bool checkWinConditionMulGame();

	//returns false in case of game over.
	bool keyboardEventHandler();
	bool processInput();
	bool processInputForSecondPlayer();
private:
	bool _processCollision(bool SnakeMovement);
	bool _processPause();
	void _stopUntillInput();
	void _setNewEngine(IEngine * newEng);
	void _switchEngine(eLib lib);
	int _winWidth = MIN_WIN_WIDTH;
	int _winHeight = MIN_WIN_HEIGHT;
	IEngine * _engine = nullptr;
	ISoundEngine *_soundEngine = nullptr;
	LibManager * _libManager = nullptr;
	Snake _snake;
	Snake _snake2;
	FoodManager _foodManager;
	ObstaclesManager _obstaclesManager;
	ScoreSystem _scoreSystem;
	ScoreSystem _scoreSystemForSecondPlayer;
	Menu _mainMenu;
	Menu _pauseMenu;
	bool _shouldOpenMenu = false;
	bool _nextLvlOn = false;
	//in milliseconds
	float _gameSpeed = DEFAULT_SPEED;
	bool _inMulMode = false;
};

#endif //NIBBLER_GAME_H
