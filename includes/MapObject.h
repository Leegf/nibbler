/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MapObject.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/27 19:54:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/08/03 15:43:17 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NIBBLER_MAPOBJECT_H
#define NIBBLER_MAPOBJECT_H

#include "Config.h"

class MapObject {
public:
	MapObject();
	~MapObject();
	MapObject(MapObject const &src) = default;
	MapObject &operator=(MapObject const &rhs) = default;

	eObject type;
	int x;
	int y;

private:

};

#endif //NIBBLER_MAPOBJECT_H
