/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Snake.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/27 20:18:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/29 15:17:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NIBBLER_SNAKE_H
#define NIBBLER_SNAKE_H

#include <vector>
#include <memory>
#include "IEngine.h"
#include "Config.h"
#include "MapObject.h"
#include "ScoreSystem.h"
#include "ISoundEngine.h"

class FoodManager;

class Snake {
public:
	Snake();
	Snake(Snake const &src) = delete;
	~Snake();
	Snake &operator=(Snake const &rhs) = delete;

	struct SnakePart {
		SnakePart(int x, int y, eObject type, eRotate dir);
		SnakePart(const SnakePart & src) = default;
		SnakePart & operator=(const SnakePart & src) = default;
		int x, y;
		eObject type;
		eRotate dir;
	};

	std::vector<std::shared_ptr<SnakePart>> parts;
	void setEngine(IEngine * engine);
	void setSoundEngine(ISoundEngine * soundEngine);
	void setScoreSystem(ScoreSystem * scoreSystem);
	void setAllPartTypes(eObject Head, eObject Body, eObject Tail, eObject BodyUpRight, eObject BodyUpLeft, eObject BodyDownRight, eObject BodyDownLeft);
	void setWinWidthAndHeight(int width, int height);
	void setSecondSnake(const Snake * snake);
	void draw() const;
	void addNewPart();
	bool move(const std::vector<MapObject> & obstacles, FoodManager & foods, eRotate inputDir);
	void positionAtCenter(int winWidth, int winHeight);
	bool repeatLastMove(const std::vector<MapObject> & obstacles, FoodManager & foods);
	void reset();

private:
	void _checkCollision(const std::vector<MapObject> & obstacles, FoodManager & foods, eObject & typeOfObjectCollision) const;
	void _moveAllParts(eRotate dir);
	void _moveHead(eRotate dir);
	void _initialize();
	IEngine * _engine;
	ISoundEngine * _soundEngine;
	ScoreSystem * _scoreSystem;
	const Snake * _secondSnake = nullptr;
	int _winHeight;
	int _winWidth;
	eObject _head;
	eObject _body;
	eObject _tail;
	eObject _bodyUpRight;
	eObject _bodyUpLeft;
	eObject _bodyDownLeft;
	eObject _bodyDownRight;
};

#endif //NIBBLER_SNAKE_H