/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Menu.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/25 15:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/15 16:58:18 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NIBBLER_MENU_H
#define NIBBLER_MENU_H

#include <vector>
#include <functional>
#include "IEngine.h"
#include "ISoundEngine.h"

class Menu {
public:
	Menu();
	Menu(Menu const &src) = delete;
	~Menu();
	Menu &operator=(Menu const &rhs) = delete;

	struct MenuEntry {
		MenuEntry(const std::string & text, int y, int x, bool selected = false, std::function<void()> func = nullptr);
		MenuEntry(const MenuEntry & src) = default;
		MenuEntry & operator=(const MenuEntry & src) = default;
		std::string text;
		int x;
		int y;
		bool selected;
		std::function<void()> func;
	};

	void tick();
	void startGame();
	void startMulGame();
	void returnToMenu();
	void endGame();
	void showText(const std::string & text);
	void drawEntries();
	bool open(opFlag & Flag);
	void setEngine(IEngine * engine);
	void setSoundEngine(ISoundEngine * soundEngine);
	void setFuncForDrawingBg(std::function<void()> func);
	void setFuncForSwitchingEngines(std::function<void(eLib lib)> func);
	void setWinAndHeight(int winWidth, int winHeight);
	void setTitleText(const std::string & text);
	void keyboardEventHandler();
	void setPosition(int x, int y);
	void setAllParameters(eColor mainColor, eColor addColor, eColor fontColor, eColor addFontColor,
			eFont Font, int FontSize);
	void setEntryParameters(int entryWidth, int entryHeight);
	void addNewEntry(const std::string & text, int x, int y, bool selected = false, std::function<void()> func = nullptr);

private:
	void _selectLowerEntry();
	void _selectUpperEntry();
	void _showGameTitle();
	void _deselectAllEntries();
	std::function<void()> _funcForDrawingBg;
	std::function<void(eLib lib)> _funcForSwitchingEngines;
	IEngine * _engine;
	ISoundEngine * _soundEngine;
	std::vector<MenuEntry> _menuEntries;
	std::vector<MenuEntry>::iterator _curEntry;
	std::string _titleText;
	int _winWidth = 0;
	int _winHeight = 0;
	eColor _mainColor;
	eColor _addColor;
	eColor _fontColor;
	eColor _addFontColor;
	eFont _font;
	int _fontSize;

	int _entryHeight = 0;
	int _entryWidth = 0;

	int _xPos;
	int _yPos;

	//operation flag
	opFlag _opFlag = opFlag::None;
};

#endif //NIBBLER_MENU_H
