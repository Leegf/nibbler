#!/bin/bash

~/.brew/bin/brew install sdl2
~/.brew/bin/brew install sdl2_image
~/.brew/bin/brew install sdl2_ttf
~/.brew/bin/brew install sdl2_mixer
~/.brew/bin/brew install sfml
~/.brew/bin/brew install glew
~/.brew/bin/brew install glfw3
~/.brew/bin/brew install freetype
~/.brew/bin/brew install glm
cd libraries/opengl_mode
git clone https://github.com/nothings/stb.git
