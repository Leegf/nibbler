.PHONY: clean fclean re

NAME  = nibbler
CXX	  = clang++
CXXFLAGS = -Wall -Werror -Wextra -std=c++14

SRCDIR = ./sources/
OBJDIR = ./objects/
INCDIR = ./includes/

SRC = main.cpp \
	 LibManager.cpp \
	 Game.cpp \
	 MapObject.cpp \
	 Snake.cpp \
	 FoodManager.cpp \
	 Menu.cpp \
	 ObstaclesManager.cpp \
	 ScoreSystem.cpp \
	 Timer.cpp

OBJ = $(addprefix $(OBJDIR), $(SRC:.cpp=.o))

INC = $(addprefix $(INCDIR), \
	 IEngine.h \
	 LibManager.h \
	 Config.h \
	 Game.h \
	 MapObject.h \
	 Snake.h \
	 FoodManager.h \
	 ISoundEngine.h \
	 Menu.h \
	 ObstaclesManager.h \
	 ScoreSystem.h \
	 Timer.h \
	 )

all: sound lib $(OBJDIR) $(NAME)

$(addprefix $(OBJDIR), %.o):$(addprefix $(SRCDIR), %.cpp) $(INC)
	$(CXX) $(CXXFLAGS) $< -c -o $@ -I $(INCDIR)

$(NAME): $(OBJ)
	$(CXX) $(CXXFLAGS) $^ -o $@

sound:
	$(MAKE) -C libraries/sound

lib:
	$(MAKE) -C libraries/sdl_mode
	$(MAKE) -C libraries/sfml_mode
#	$(MAKE) -C libraries/opengl_mode

$(OBJDIR):
	mkdir $(OBJDIR)

clean:
	rm -rf $(OBJDIR)
	$(MAKE) -C libraries/sdl_mode clean
	$(MAKE) -C libraries/sfml_mode clean
	$(MAKE) -C libraries/opengl_mode clean
	$(MAKE) -C libraries/sound clean

fclean:
	rm -rf $(OBJDIR) $(NAME)
	$(MAKE) -C libraries/sdl_mode fclean
	$(MAKE) -C libraries/sfml_mode fclean
	$(MAKE) -C libraries/opengl_mode fclean
	$(MAKE) -C libraries/sound fclean

re: fclean all
	$(MAKE) -C libraries/sdl_mode re
	$(MAKE) -C libraries/sfml_mode re
	$(MAKE) -C libraries/opengl_mode re
	$(MAKE) -C libraries/sound re

