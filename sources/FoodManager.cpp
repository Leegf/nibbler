/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FoodManager.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 14:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/07 15:57:06 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <random>
#include "FoodManager.h"
#include "Snake.h"

FoodManager::FoodManager() {
	_initializeProperties();
}

FoodManager::~FoodManager() {
}

void FoodManager::_initializeProperties() {
	_food.type = eObject::Food;
	_superFood.type = eObject::SuperFood;
}

void FoodManager::setWinAndHeight(int winWidth, int winHeight) {
	_winWidth = winWidth;
	_winHeight = winHeight;
}

void FoodManager::RefreshFood() {
	_spawnRandomlyFood(&_food);
}

void FoodManager::RefreshSuperFood() {
	_removeSuperFood();
//	_spawnRandomlyFood(&_superFood);
}

void FoodManager::_spawnRandomlyFood(MapObject * food) {
	std::default_random_engine generator( std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<int> ranWidth(0, _winWidth - 1);
	std::uniform_int_distribution<int> ranHeight(0, _winHeight - 1);
	do {
		food->x = ranWidth(generator);
		food->y = ranHeight(generator);
	} while (!_checkCollision(*food));

	if (food->type == eObject::SuperFood)
		_timerForRemovingSuperFood.add(std::chrono::seconds(SUPERFOOD_LIFETIME), std::bind(&FoodManager::_removeSuperFood, this));
}

void FoodManager::setObjectsToCheckCollisionWith(const Snake *snake,
												 const std::vector<MapObject> *obstacles) {
	_snake = snake;
	_obstacles = obstacles;
}

void FoodManager::setObjectsToCheckCollisionWith(const Snake *snake,
												 const Snake *snake2,
												 const std::vector<MapObject> *obstacles) {
	_snake = snake;
	_snake2 = snake2;
	_obstacles = obstacles;
}

bool FoodManager::_checkCollision(const MapObject &food) const {
	for (const auto & i : _snake->parts)
	{
		if (food.x == i->x && food.y == i->y)
			return false;
	}
	//for second player
	if (_snake2)
	{
		for (const auto & i : _snake2->parts)
		{
			if (food.x == i->x && food.y == i->y)
				return false;
		}
	}

	for (const auto & i : *_obstacles)
	{
		if (food.x == i.x && food.y == i.y)
			return false;
	}

	//for checking different food types with each other
	if ((food.type == _food.type && food.x == _superFood.x && food.y == _superFood.y)
	|| (food.type == _superFood.type && food.x == _food.x && food.y == _food.y))
		return false;

	return true;
}

void FoodManager::draw() {
	_engine->drawSprite(_food.type, eRotate::Up, _food.x, _food.y);
	if ( _superFood.x == _winWidth + 1 && _superFood.y == _winHeight + 1)
		return ;
	_engine->drawSprite(_superFood.type, eRotate::Up, _superFood.x, _superFood.y);
}

void FoodManager::setEngine(IEngine *engine) {
	_engine = engine;
}

const MapObject &FoodManager::getFood() const {
	return _food;
}

const MapObject &FoodManager::getSuperFood() const {
	return _superFood;
}

void FoodManager::_removeSuperFood() {
	_superFood.x = _winWidth + 1;
	_superFood.y = _winHeight + 1;
}

void FoodManager::Start() {
	RefreshFood();
	RefreshSuperFood();
	_timer.add(std::chrono::seconds(SUPERFOOD_SPAWN_INTERVAL), std::bind(&FoodManager::_spawnRandomlyFood, this, &_superFood), true);
}

void FoodManager::reset() {
	Start();
}

