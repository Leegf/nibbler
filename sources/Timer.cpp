/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Timer.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:33:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/07 15:52:50 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Timer.h"
#include <thread>

	Timer::Timer() : _isRunning(false), _isLooping(false) {
		std::thread([=]() {
			while (true) {
				std::this_thread::sleep_for(std::chrono::milliseconds(100));
				if (_isRunning &&
						std::chrono::high_resolution_clock::now() - _start > _delay) {
					{
						std::lock_guard<std::mutex> lock(_mChanging);
						_isRunning = false;
						_callback();
					}
					if (_isLooping)
						add(_delay, _callback, _isLooping);
				}
			}
	}).detach();
}


void Timer::add(std::chrono::seconds delay, std::function<void()> callback, bool isLooping) {
	std::lock_guard<std::mutex> lock(_mChanging);
	_callback = callback;
	_isRunning = true;
	_isLooping = isLooping;
	_start = std::chrono::high_resolution_clock::now();
	_delay = delay;
}
