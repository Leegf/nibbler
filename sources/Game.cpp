/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Game.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/27 17:26:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/10/19 18:01:07 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game.h"
#include <chrono>
#include <zconf.h>

//special
Game::Game() {
}

Game::Game(int winWidth, int winHeight) : _winWidth(winWidth), _winHeight(winHeight) {
	PreloadGame(winWidth, winHeight);
}
Game::~Game() {
}

void Game::PreloadGame(int winWidth, int winHeight) {
	if (_libManager && !_soundEngine && !_engine)
	{
		_engine = _libManager->createEngine(eLib::Sdl2);
		_soundEngine = _libManager->createSoundEngine();
	}
	_gameSpeed = DEFAULT_SPEED;
	_winWidth = winWidth;
	_winHeight = winHeight;
	_engine->createWindow(_winWidth * SPRITE_SIZE, _winHeight * SPRITE_SIZE);
	_snake.setEngine(_engine);
	_snake.setAllPartTypes(eObject::Head, eObject::Body, eObject::Tail, eObject::BodyUpRight, eObject::BodyUpLeft, eObject::BodyDownRight, eObject::BodyDownLeft);
	_snake.setSoundEngine(_soundEngine);
	_snake.setWinWidthAndHeight(winWidth, winHeight);
	_snake.positionAtCenter(_winWidth, _winHeight);
	_snake.setScoreSystem(&_scoreSystem);
	_foodManager.setEngine(_engine);
	_obstaclesManager.setEngine(_engine);
	_scoreSystem.setEngine(_engine);
	_scoreSystem.setParameters(eFont::Naruto, eColor::Blue, SCORE_FONT_SIZE, 1, _winWidth - 5);
	_scoreSystem.setPlayerName("Naruto");
	_foodManager.setObjectsToCheckCollisionWith(&_snake, &_obstaclesManager.obstacles);
	_foodManager.setWinAndHeight(winWidth, winHeight);
	_obstaclesManager.setObjectsToCheckCollisionWith(&_snake, &_foodManager);
	_obstaclesManager.setWinAndHeight(winWidth, winHeight);
	_foodManager.Start();
	_obstaclesManager.spawnRandomlyObstacles(NUM_OF_OBSTACLES);
	initializeMainMenu();
	initializePauseMenu();
	//needed?
}

#define TICK_INTERVAL 100

void Game::Tick() {
	while (true) {
		auto start = std::chrono::steady_clock::now();
		if (!processInput())
			break ;
		_engine->clearWindow();
		drawDifferentBgForEngines();
		drawMapObjects();
		_scoreSystem.draw();
		_snake.draw();
		_engine->updateWindow();
		//false if game is won
		if (!checkScoreAndRunNextLvl())
			break ;
		//makes sure tick inteval is at least TICK_INTERVAL miliseconds
		while (true)
		{
//			if (std::chrono::duration_cast<std::chrono::milliseconds>(start.time_since_epoch()).count() <= TICK_INTERVAL)
			if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() <= _gameSpeed)
				continue;
			else
				break;
		}
	}
}

void Game::TickForMulGame() {
	while (true) {
		auto start = std::chrono::steady_clock::now();
		if (!processInput())
		{
			gameWon(false);
			break;
		}
		if (!processInputForSecondPlayer())
		{
			gameWon(true);
			break;
		}
		_engine->clearWindow();
		drawDifferentBgForEngines();
		drawMapObjects();
		_scoreSystem.draw();
		_scoreSystemForSecondPlayer.draw();
		_snake.draw();
		_snake2.draw();
		_engine->updateWindow();
		//false if game is won
		if (checkWinConditionMulGame())
			break ;
		//makes sure tick inteval is at least TICK_INTERVAL miliseconds
		while (true)
		{
//			if (std::chrono::duration_cast<std::chrono::milliseconds>(start.time_since_epoch()).count() <= TICK_INTERVAL)
			if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() <= _gameSpeed)
				continue;
			else
				break;
		}
	}
}


void Game::drawMapObjects() {
	_foodManager.draw();
	_obstaclesManager.draw();
}

void Game::gameOver() {
	_engine->clearWindow();
	_soundEngine->playSound(eSound::GameOver);
	drawDifferentBgForEngines();
	std::string tmpScore = std::string("SCORE: ") + std::to_string(_scoreSystem.getScore());
	std::string tmpLevel = std::string("LEVEL: ") + std::to_string(_scoreSystem.getLvl());
	_engine->drawText(eColor::Black, eFont::Naruto, _winWidth / 2 - 6, 1, GAMEOVER_FONT_SIZE, "GAME OVER");
	_engine->drawText(eColor::Black, eFont::Naruto, _winWidth / 2 - 6, 3, GAMEOVER_FONT_SIZE, tmpScore);
	_engine->drawText(eColor::Black, eFont::Naruto, _winWidth / 2 - 6, 5, GAMEOVER_FONT_SIZE, tmpLevel);
	_engine->updateWindow();
	_stopUntillInput();
	_shouldOpenMenu = true;
}

void Game::setEngine(IEngine *eng) {
	_engine = eng;
}

void Game::setSoundEngine(ISoundEngine *eng) {
	_soundEngine = eng;
}

void Game::run() {
	opFlag flagFromMenu;
	//works well
	_soundEngine->playMusic(eMusic::Konoha);
	do {
		_shouldOpenMenu = false;
		if (_mainMenu.open(flagFromMenu))
		{
			if (flagFromMenu == opFlag::StartMulGame)
			{
				initializeSecondPlayer();
				TickForMulGame();
			}
			else
				Tick();
		}
		reset(true, true);
	} while (_shouldOpenMenu);
}

bool Game::processInput() {
	//at least two events: game over screen and in-game menu should be implemented
	bool resOfKeyboardEventHandler = keyboardEventHandler();
	return resOfKeyboardEventHandler;
}


bool Game::keyboardEventHandler() {
	switch (_engine->keyBoardEvent()) {
		case eKey::Quit:
			return false;
		case eKey::MoveRight:
			return _processCollision(_snake.move(_obstaclesManager.obstacles, _foodManager, eRotate::Right));
		case eKey::MoveLeft:
			return _processCollision(_snake.move(_obstaclesManager.obstacles, _foodManager, eRotate::Left));
		case eKey::MoveUp:
			return _processCollision((_snake.move(_obstaclesManager.obstacles, _foodManager, eRotate::Up)));
		case eKey::MoveDown:
			return _processCollision(_snake.move(_obstaclesManager.obstacles, _foodManager, eRotate::Down));
		case eKey::Pause:
			return _processPause();
		case eKey::Enter:
			return _processCollision(_snake.repeatLastMove(_obstaclesManager.obstacles, _foodManager));
		case eKey::Sasuke:
			_soundEngine->playSound(eSound::Sasuke);
			return _processCollision(_snake.repeatLastMove(_obstaclesManager.obstacles, _foodManager));
		case eKey::None:
			return _processCollision(_snake.repeatLastMove(_obstaclesManager.obstacles, _foodManager));
		case eKey::Sdl:
			_switchEngine(eLib::Sdl2);
			return _processCollision(_snake.repeatLastMove(_obstaclesManager.obstacles, _foodManager));
		case eKey::Sfml:
			_switchEngine(eLib::Sfml);
			return _processCollision(_snake.repeatLastMove(_obstaclesManager.obstacles, _foodManager));
		case eKey::Opengl:
			_switchEngine(eLib::Opengl);
			return _processCollision(_snake.repeatLastMove(_obstaclesManager.obstacles, _foodManager));
		default:
			return false;
	}
	return false;
}

bool Game::processInputForSecondPlayer() {
	if (_engine->keyBoardEvent(eKey::W))
		return _processCollision(_snake2.move(_obstaclesManager.obstacles, _foodManager, eRotate::Up));
	else if (_engine->keyBoardEvent(eKey::D))
		return _processCollision(_snake2.move(_obstaclesManager.obstacles, _foodManager, eRotate::Right));
	else if (_engine->keyBoardEvent(eKey::S))
		return _processCollision(_snake2.move(_obstaclesManager.obstacles, _foodManager, eRotate::Down));
	else if (_engine->keyBoardEvent(eKey::A))
		return _processCollision(_snake2.move(_obstaclesManager.obstacles, _foodManager, eRotate::Left));
	else
		return _processCollision(_snake2.repeatLastMove(_obstaclesManager.obstacles, _foodManager));
}


void Game::initializeMainMenu() {
	_mainMenu.setEngine(_engine);
	_mainMenu.setSoundEngine(_soundEngine);
	_mainMenu.setFuncForDrawingBg(std::bind(&Game::drawDifferentBgForEngines, this));
	_mainMenu.setFuncForSwitchingEngines(std::bind(&Game::_switchEngine, this, std::placeholders::_1));
	_mainMenu.setPosition(_winWidth / 2 - 5, _winHeight / 2);
	_mainMenu.setEntryParameters(10, 2);
	_mainMenu.setWinAndHeight(_winWidth, _winHeight);
	_mainMenu.setAllParameters(eColor::Black, eColor::White, eColor::Blue, eColor::Black, eFont::Naruto, MENU_FONT_SIZE);
	_mainMenu.setTitleText(TITLE_NAME);

	_mainMenu.addNewEntry("START", 4, 1, true, std::bind(&Menu::startGame, &_mainMenu));
	_mainMenu.addNewEntry("MULTIPLAYER", 3, 1, true, std::bind(&Menu::startMulGame, &_mainMenu));
	_mainMenu.addNewEntry("PROLOGUE", 3, 1, false, std::bind(&Menu::showText, &_mainMenu, "Sasuke Uchiha (Uchiha Sasuke) is one of the last surviving\n members of Konohagakure's Uchiha clan. After his older brother, Itachi, slaughtered\n their clan, Sasuke made it his mission in life to avenge them by killing Itachi.\n He is added to Team 7 upon becoming a ninja and, through competition with his rival\n and best friend, Naruto Uzumaki, Sasuke starts developing his skills.\n Dissatisfied with his progress, he defects from Konoha so that he can acquire\n the strength needed to exact his revenge. His years of seeking vengeance and his\n actions that followed become increasingly demanding, irrational and isolates him\n from others, leading him to be branded as an international criminal.............."));
	_mainMenu.addNewEntry("CONTROLS", 3, 1, false, std::bind(&Menu::showText, &_mainMenu, "arrows to move, q to quit, escape to press pause, space to perform SASUKE"));
	_mainMenu.addNewEntry("CREDITS", 3, 1, false, std::bind(&Menu::showText, &_mainMenu, "made by Kazekage(Yulia aka the great estonyec)\n and by Hokage(Leonid aka penguin god)"));
	_mainMenu.addNewEntry("EXIT", 4, 1,false, std::bind(&Menu::endGame, &_mainMenu));
}

void Game::initializePauseMenu() {
	_pauseMenu.setEngine(_engine);
	_pauseMenu.setSoundEngine(_soundEngine);
	_pauseMenu.setFuncForDrawingBg(std::bind(&Game::drawDifferentBgForEngines, this));
	_pauseMenu.setFuncForSwitchingEngines(std::bind(&Game::_switchEngine, this, std::placeholders::_1));
	_pauseMenu.setPosition(_winWidth / 2 - 4, _winHeight / 2 + 2);
	_pauseMenu.setEntryParameters(10, 2);
	_pauseMenu.setWinAndHeight(_winWidth, _winHeight);
	_pauseMenu.setTitleText("Pause");
	_pauseMenu.setAllParameters(eColor::Black, eColor::White, eColor::Blue, eColor::Black, eFont::Naruto, MENU_FONT_SIZE);
	_pauseMenu.addNewEntry("RESUME", 3, 1, true, std::bind(&Menu::startGame, &_pauseMenu));
	_pauseMenu.addNewEntry("MAIN MENU", 2, 1, false, std::bind(&Menu::returnToMenu, &_pauseMenu));
	_pauseMenu.addNewEntry("EXIT", 4, 1, false, std::bind(&Menu::endGame, &_pauseMenu));
}

void Game::reset(bool WithLevel, bool withLvlModifications) {
	_inMulMode = false;
	_nextLvlOn = false;
	_snake.reset();
	_snake2.reset();
	_foodManager.reset();
	_obstaclesManager.reset();
	_scoreSystem.resetScore();
	_scoreSystemForSecondPlayer.resetScore();
	_scoreSystem.setSecondPlayerMode(false);
	_scoreSystemForSecondPlayer.setSecondPlayerMode(false);
	_foodManager.setObjectsToCheckCollisionWith(&_snake, nullptr, &_obstaclesManager.obstacles);
	_obstaclesManager.setObjectsToCheckCollisionWith(&_snake, nullptr, &_foodManager);
	if (WithLevel)
		_scoreSystem.resetLvl();
	if (withLvlModifications)
	{
		_obstaclesManager.spawnRandomlyObstacles(NUM_OF_OBSTACLES);
		_gameSpeed = DEFAULT_SPEED;
	}
}

bool Game::_processCollision(bool SnakeMovement) {
	if (!SnakeMovement) {
		_shouldOpenMenu = true;
		if (!_inMulMode)
			gameOver();
	}
	return SnakeMovement;
}

bool Game::_processPause() {
	bool retVal;
	opFlag flag;
	retVal = _pauseMenu.open(flag);
	if (flag == opFlag::MainMenu) {
		_shouldOpenMenu = true;
	}
	return retVal;
}

bool Game::checkScoreAndRunNextLvl() {
	if (_nextLvlOn)
	{
		_scoreSystem.nextLvl();
		if (_scoreSystem.getLvl() == WIN_LVL) {
			gameWon();
			return false;
		}
		_soundEngine->playSound(eSound::NewLvl);
		_stopUntillInput();
		_nextLvlOn = false;
	}
	if (_scoreSystem.getScore() >= LVL_UP_SCORE)
	{
		reset();
		_obstaclesManager.spawnRandomlyObstacles(_obstaclesManager.obstacles.size() + OBSTACLES_STEP);
		_gameSpeed -= SPEED_STEP;
		_nextLvlOn = true;
	}
	return true;
}

void Game::_stopUntillInput() {
	while (true)
	{
		eKey hm = _engine->keyBoardEvent();
		if (hm == eKey::Enter || hm == eKey::Pause || hm == eKey::Quit || hm == eKey::Sasuke)
			break ;
	}
}

void Game::gameWon() {
	_engine->clearWindow();
	_soundEngine->playSound(eSound::GameWon);
	_engine->drawBackground(eColor::Purple);
	_engine->drawText(eColor::Black, eFont::Naruto, _winWidth / 2 - 10, 1, GAMEOVER_FONT_SIZE, "GAME WON!!!..");
	_engine->drawText(eColor::Black, eFont::Naruto, _winWidth / 2 - 10, 4, GAMEOVER_FONT_SIZE, "SASUKE RESCUED");
	_engine->updateWindow();
	_stopUntillInput();
	_shouldOpenMenu = true;
}

void Game::setLibManager(LibManager *manager) {
	_libManager = manager;
}

void Game::_setNewEngine(IEngine *newEng) {
	setEngine(newEng);
	_snake.setEngine(newEng);
	_snake2.setEngine(newEng);
	_foodManager.setEngine(newEng);
	_obstaclesManager.setEngine(newEng);
	_scoreSystem.setEngine(newEng);
	_scoreSystemForSecondPlayer.setEngine(newEng);
	_mainMenu.setEngine(newEng);
	_pauseMenu.setEngine(newEng);
}

void Game::drawDifferentBgForEngines() {
	switch (_libManager->curLib) {
		case eLib::Sdl2:
			_engine->drawBackground(eColor::Purple);
			break;
		case eLib::Sfml:
			_engine->drawBackground(eColor::Orange);
			break;
		case eLib::Opengl:
			_engine->drawBackground(eColor::Green);
			break;
		default:
			_engine->drawBackground(eColor::Purple);
	}
}

void Game::_switchEngine(eLib lib) {
	if (lib == _libManager->curLib)
		return ;
	_setNewEngine(_libManager->changeEngine(lib));
	_engine->createWindow(_winWidth * SPRITE_SIZE, _winHeight * SPRITE_SIZE);
}

void Game::initializeSecondPlayer() {
	_snake2.setEngine(_engine);
	_snake2.setAllPartTypes(eObject::Head2, eObject::Body2, eObject::Tail2, eObject::BodyUpRight2, eObject::BodyUpLeft2, eObject::BodyDownRight2, eObject::BodyDownLeft2);
	_snake2.setSoundEngine(_soundEngine);
	_snake2.setWinWidthAndHeight(_winWidth, _winHeight);
	_snake2.positionAtCenter(_winWidth - 5, _winHeight);
	_snake2.setScoreSystem(&_scoreSystemForSecondPlayer);
	_snake2.setSecondSnake(&_snake);
	_scoreSystem.setSecondPlayerMode(true);
	_scoreSystemForSecondPlayer.setEngine(_engine);
	_scoreSystemForSecondPlayer.setParameters(eFont::Naruto, eColor::Blue, SCORE_FONT_SIZE, 1, 0);
	_scoreSystemForSecondPlayer.setSecondPlayerMode(true);
	_scoreSystemForSecondPlayer.setPlayerName("Sakura");
	_snake.setSecondSnake(&_snake2);
	_inMulMode = true;
	//TODO: check managers
	_foodManager.setObjectsToCheckCollisionWith(&_snake, &_snake2, &_obstaclesManager.obstacles);
	_obstaclesManager.setObjectsToCheckCollisionWith(&_snake, &_snake2, &_foodManager);
}

bool Game::checkWinConditionMulGame() {
	if (_scoreSystemForSecondPlayer.getScore() >= MUL_WIN_SCORE)
	{
		gameWon(false);
		return true;
	}
	else if (_scoreSystem.getScore() >= MUL_WIN_SCORE)
	{
		gameWon(true);
		return true;
	}
	return false;
}

void Game::gameWon(bool FirstPlayer) {
	_engine->clearWindow();
	_soundEngine->playSound(eSound::GameWon);
	drawDifferentBgForEngines();
	if (FirstPlayer)
		_engine->drawText(eColor::Black, eFont::Naruto, _winWidth / 2 - 10, 1, GAMEOVER_FONT_SIZE, "NARUTO WON!!!..");
	else
		_engine->drawText(eColor::Black, eFont::Naruto, _winWidth / 2 - 10, 1, GAMEOVER_FONT_SIZE, "SAKURA WON!!!..");
	_engine->updateWindow();
	_stopUntillInput();
	_shouldOpenMenu = true;
}

