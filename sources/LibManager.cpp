#include "LibManager.h"
#include <dlfcn.h>
#include "ISoundEngine.h"

IEngine *LibManager::createEngine(eLib lib) {

	curLib = lib;
	_dlHandle = dlopen(LibPath[static_cast<int>(lib)].c_str(), RTLD_LAZY);
	if (!_dlHandle) {
		throw std::runtime_error(std::string("Error occurred in dlopen: ") + dlerror());
	}

	_createEngine = (create_func)dlsym(_dlHandle, "create");
	_destroyEngine = (destroy_func)dlsym(_dlHandle, "destroy");

	const char *error = dlerror();
	if (error) {
		throw std::runtime_error(std::string("Error occurred in dlopen: ") + error);
	}

	_engine = _createEngine();

	return _engine;
}

ISoundEngine *LibManager::createSoundEngine() {

	_dlSoundHandle = dlopen("libraries/sound/libsound.so", RTLD_LAZY);
	if (!_dlSoundHandle) {
		throw std::runtime_error(std::string("Error occurred in dlopen: ") + dlerror());
	}

	_createSoundEngine = (create_sound_func)dlsym(_dlSoundHandle, "create");
	_destroySoundEngine = (destroy_sound_func)dlsym(_dlSoundHandle, "destroy");

	const char *error = dlerror();
	if (error) {
		throw std::runtime_error(std::string("Error occurred in dlopen: ") + error);
	}

	_soundEngine = _createSoundEngine();

	return _soundEngine;
}

void LibManager::destroyEngine() {

	_destroyEngine(_engine);
	dlclose(_dlHandle);
}

IEngine *LibManager::changeEngine(eLib lib) {

    destroyEngine();
    return createEngine(lib);
}

void LibManager::destroySoundEngine() {

    _destroySoundEngine(_soundEngine);
    dlclose(_dlSoundHandle);
}
