/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScoreSystem.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 17:43:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/29 17:20:06 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScoreSystem.h"

ScoreSystem::ScoreSystem() {
}

ScoreSystem::~ScoreSystem() {
}

void ScoreSystem::draw() {
	std::string tmpScore =
			std::string("Score: ") + std::to_string(_currentScore);
	if (_secondPlayer)
	{
		_engine->drawText(_color, _font, _xPos, _yPos, _fontSize, _playerName);
		_engine->drawText(_color, _font, _xPos, _yPos + 1, _fontSize, tmpScore);
	}
	else
	{
		std::string tmpCurLvl = std::string("Level: ") + std::to_string(_curLvl);
		_engine->drawText(_color, _font, _xPos, _yPos, _fontSize, tmpScore);
		_engine->drawText(_color, _font, _xPos, _yPos + 1, _fontSize, tmpCurLvl);
	}
}

void ScoreSystem::setParameters(eFont font, eColor color, int FontSize, int yPos,
								int xPos) {
	_font = font;
	_color = color;
	_fontSize = FontSize;
	_xPos = xPos;
	_yPos = yPos;
}

void ScoreSystem::setEngine(IEngine *engine) {
	_engine = engine;
}

void ScoreSystem::scoreBonus() {
	_currentScore+=FOOD_VALUE;
}

void ScoreSystem::scoreSuperBonus() {
	_currentScore+=SUPERFOOD_VALUE;
}

void ScoreSystem::resetScore() {
	_currentScore = 0;
}

int ScoreSystem::getScore() const {
	return _currentScore;
}

int ScoreSystem::getLvl() const {
	return _curLvl;
}

void ScoreSystem::nextLvl() {
	_curLvl++;
}

void ScoreSystem::resetLvl() {
	_curLvl = 1;
}

void ScoreSystem::setSecondPlayerMode(bool flag) {
	_secondPlayer = flag;
}

void ScoreSystem::setPlayerName(const std::string &name) {
	_playerName = name;
}

