/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Snake.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/27 20:18:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/29 15:23:40 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <zconf.h> /* has to be deleted */
#include "Snake.h"
#include "FoodManager.h"

Snake::Snake() {
}

Snake::~Snake() {
}

void Snake::addNewPart() {
	parts.insert(std::prev(parts.end()), std::make_shared<SnakePart>(SnakePart(**std::prev(parts.end()))));
}

bool Snake::move(const std::vector<MapObject> & obstacles, FoodManager & foods, eRotate inputDir) {
	eObject collisionType;

	_moveAllParts(inputDir);
	_checkCollision(obstacles, foods, collisionType);
	if (collisionType == eObject::Obstacle) {
		return false;
	}
	else if (collisionType == eObject::Food) {
		_soundEngine->playSound(eSound::Naruto);
		addNewPart();
		foods.RefreshFood();
		_scoreSystem->scoreBonus();
		return true;
	}
	else if (collisionType == eObject::SuperFood) {
		_soundEngine->playSound(eSound::Ramen);
		addNewPart();
		addNewPart();
		addNewPart();
		foods.RefreshSuperFood();
		_scoreSystem->scoreSuperBonus();
		return true;
	}
	return true;
}

#define HEAD_PART (*parts.begin())
#define AFTERHEAD_PART (*std::next(parts.begin()))
#define TAIL_PART (*std::prev(parts.end()))

void Snake::_moveAllParts(eRotate inputDir) {

	for (auto it = parts.rbegin(); it != std::prev(parts.rend()); it++)
		**it = **(std::next(it));
	TAIL_PART->type = _tail;
	TAIL_PART->dir = (*std::next(parts.rbegin()))->dir;
	AFTERHEAD_PART->type = _body;
	_moveHead(inputDir);
}

void Snake::_moveHead(eRotate inputDir) {
	switch(inputDir) {

		case eRotate::Up:
			switch(HEAD_PART->dir) {
				case eRotate::Up:
					HEAD_PART->y--;
					break;
				case eRotate::Down:
					HEAD_PART->y++;
					break;
				case eRotate::Left:
					HEAD_PART->y--;
					HEAD_PART->dir = eRotate::Up;
					AFTERHEAD_PART->type = _bodyDownRight;
					break;
				case eRotate::Right:
					HEAD_PART->y--;
					HEAD_PART->dir = eRotate::Up;
					AFTERHEAD_PART->type = _bodyDownLeft;
					break;
			}
			break;

		case eRotate::Down:
			switch(HEAD_PART->dir) {
				case eRotate::Up:
					HEAD_PART->y--;
					break;
				case eRotate::Down:
					HEAD_PART->y++;
					break;
				case eRotate::Left:
					HEAD_PART->y++;
					HEAD_PART->dir = eRotate::Down;
					AFTERHEAD_PART->type = _bodyUpRight;
					break;
				case eRotate::Right:
					HEAD_PART->y++;
					HEAD_PART->dir = eRotate::Down;
					AFTERHEAD_PART->type = _bodyUpLeft;
					break;
			}
			break;

		case eRotate::Left:
			switch(HEAD_PART->dir) {
				case eRotate::Up:
					HEAD_PART->x--;
					HEAD_PART->dir = eRotate::Left;
					AFTERHEAD_PART->type = _bodyUpLeft;
					break;
				case eRotate::Down:
					HEAD_PART->x--;
					HEAD_PART->dir = eRotate::Left;
					AFTERHEAD_PART->type = _bodyDownLeft;
					break;
				case eRotate::Left:
					HEAD_PART->x--;
					break;
				case eRotate::Right:
					HEAD_PART->x++;
					break;
			}
			break;

		case eRotate::Right:
			switch(HEAD_PART->dir) {
				case eRotate::Up:
					HEAD_PART->x++;
					HEAD_PART->dir = eRotate::Right;
					AFTERHEAD_PART->type = _bodyUpRight;
					break;
				case eRotate::Down:
					HEAD_PART->x++;
					HEAD_PART->dir = eRotate::Right;
					AFTERHEAD_PART->type = _bodyDownRight;
					break;
				case eRotate::Left:
					HEAD_PART->x--;
					break;
				case eRotate::Right:
					HEAD_PART->x++;
					break;
			}
			break;

		default:
			break;
	}
}

void Snake::_checkCollision(const std::vector<MapObject> & obstacles, FoodManager & foods, eObject &typeOfObjectCollision) const {

	for (const auto & j : parts)
	{
		if (HEAD_PART != j && HEAD_PART->x == j->x && HEAD_PART->y == j->y)
		{
			typeOfObjectCollision = eObject::Obstacle;
			return ;
		}
	}
	//secondPlayer
	if (_secondSnake)
	{
		for (const auto & i : _secondSnake->parts)
		{
			if (HEAD_PART->x == i->x && HEAD_PART->y == i->y)
			{
				typeOfObjectCollision = eObject::Obstacle;
				return ;
			}
		}
	}
	for (const auto & j : obstacles)
	{
		if (HEAD_PART->x == j.x && HEAD_PART->y == j.y)
		{
			typeOfObjectCollision = eObject::Obstacle;
			return ;
		}
	}

	if (HEAD_PART->x == foods.getFood().x && HEAD_PART->y == foods.getFood().y) {
		typeOfObjectCollision = eObject::Food;
		return ;
	}
	if (HEAD_PART->x == foods.getSuperFood().x && HEAD_PART->y == foods.getSuperFood().y) {
		typeOfObjectCollision = eObject::SuperFood;
		return ;
	}
	if (HEAD_PART->x < 0 || HEAD_PART->y < 0 || HEAD_PART->x >= _winWidth
		|| HEAD_PART->y >= _winHeight) {
		typeOfObjectCollision = eObject::Obstacle;
		return;
	}
	typeOfObjectCollision = eObject::None;
}

void Snake::draw() const {
	for (const auto & i : parts) {
		//there're are set the way we shouldn't rotate them in any wayg
		if ((i->type == _bodyDownLeft || i->type == _bodyDownRight
			|| i->type == _bodyUpLeft || i->type == _bodyUpRight))
			_engine->drawSprite(i->type, eRotate::Up, i->x, i->y);
		else
			_engine->drawSprite(i->type, i->dir, i->x, i->y);
	}
}

void Snake::setEngine(IEngine * engine) {
	_engine = engine;
}

void Snake::positionAtCenter(int winWidth, int winHeight) {
	int xCenter = winWidth/2, yCenter = winHeight/2;
	for (auto & i : parts) {
		i->x = xCenter;
		i->y = yCenter++;
	}
}

bool Snake::repeatLastMove(const std::vector<MapObject> &obstacles,
						   FoodManager &foods) {
	return (move(obstacles, foods, HEAD_PART->dir));
}

void Snake::setWinWidthAndHeight(int width, int height) {
	_winWidth = width;
	_winHeight = height;
}

void Snake::setScoreSystem(ScoreSystem *scoreSystem) {
	_scoreSystem = scoreSystem;
}

void Snake::reset() {
	parts.clear();
	_initialize();
	positionAtCenter(_winWidth, _winHeight);
}

void Snake::_initialize() {
	parts.push_back(std::make_shared<SnakePart>(SnakePart(0, 0, _head, eRotate::Up)));
	parts.push_back(std::make_shared<SnakePart>(SnakePart(0, 0, _body, eRotate::Up)));
	parts.push_back(std::make_shared<SnakePart>(SnakePart(0, 0, _body, eRotate::Up)));
	parts.push_back(std::make_shared<SnakePart>(SnakePart(0, 0, _tail, eRotate::Up)));
}

void Snake::setSoundEngine(ISoundEngine * soundEngine) {
	_soundEngine = soundEngine;
}

void Snake::setAllPartTypes(eObject Head, eObject Body, eObject Tail,
							eObject BodyUpRight, eObject BodyUpLeft,
							eObject BodyDownRight, eObject BodyDownLeft) {
	if (!parts.empty())
		parts.clear();
	_head = Head;
	_body = Body;
	_tail = Tail;
	_bodyUpRight = BodyUpRight;
	_bodyUpLeft = BodyUpLeft;
	_bodyDownRight = BodyDownRight;
	_bodyDownLeft = BodyDownLeft;
	_initialize();
}

void Snake::setSecondSnake(const Snake *snake) {
	_secondSnake = snake;
}


Snake::SnakePart::SnakePart(int x, int y, eObject type, eRotate dir) : x(x), y(y), type(type), dir(dir) { }

