/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/27 16:43:03 by lburlach          #+#    #+#             */
/*   Updated: 2019/10/19 14:17:57 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Config.h"
#include "Game.h"
#include "LibManager.h"

bool littleParser(int ac, char **av)
{
	if (ac != 3) {
		std::cerr<< "You should provide executable with two parameters: width and height\n";
		return false;
	}

	int width, height;
	try {
		width = std::stoi(std::string(av[1]));
		height = std::stoi(std::string(av[2]));
	}
	catch(std::exception & e) {
		std::cerr<<"Exception caught processing parameters, "<<e.what()<<"\n";
		return false;
	}

	if ((std::to_string(width).size() != std::string(av[1]).size())
		|| (std::to_string(height).size() != std::string(av[2]).size()))
	{
		std::cerr<<"only numbers are allowed\n";
		return false;
	}

	if (width < MIN_WIN_WIDTH || width > MAX_WIN_WIDTH) {
		std::cerr<<"width should be in range of between "<<MIN_WIN_WIDTH<<" and "<<MAX_WIN_WIDTH<<"\n";
		return false;
	}
	if (height < MIN_WIN_HEIGHT || height > MAX_WIN_HEIGHT) {
		std::cerr<<"height should be in range of between "<<MIN_WIN_HEIGHT<<" and "<<MAX_WIN_HEIGHT<<"\n";
		return false;
	}
	return true;
}


int main(int ac, char **av) {

    if (!littleParser(ac, av)) {
        return EXIT_FAILURE;
    }

    try
    {
        //create new game
        LibManager manager;

        IEngine *engine = manager.createEngine(eLib::Sfml);
        ISoundEngine *soundEngine = manager.createSoundEngine();

        Game game;
        game.setSoundEngine(soundEngine);
        game.setEngine(engine);
        game.setLibManager(&manager);
        game.PreloadGame(std::stoi(av[1]), std::stoi(av[2]));
        game.run();
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
	return EXIT_SUCCESS;
}
