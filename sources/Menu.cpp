/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Menu.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/25 15:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/10/19 17:59:08 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sstream>
#include <utility>
#include <chrono>
#include "Menu.h"

Menu::Menu() {

}

Menu::~Menu() {

}

void Menu::setEngine(IEngine *engine) {
	_engine = engine;
}

void Menu::setWinAndHeight(int winWidth, int winHeight) {
	_winWidth = winWidth;
	_winHeight = winHeight;
}

bool Menu::open(opFlag & Flag) {
	_opFlag = opFlag::None;
	_deselectAllEntries();
	_curEntry = _menuEntries.begin();
	tick();
	Flag = _opFlag;
	if (_opFlag == opFlag::StartGame || _opFlag == opFlag::StartMulGame)
		return true;
	else if (_opFlag == opFlag::EndGame || _opFlag == opFlag::MainMenu)
		return false;
	return false;
}

void Menu::tick() {
	while (true)
	{
		auto start = std::chrono::steady_clock::now();
		_engine->clearWindow();
//		_engine->drawBackground(eColor::Purple);
		_funcForDrawingBg();
		keyboardEventHandler();
		if (_opFlag == opFlag::ShowingText)
			_curEntry->func();
		else {
			if (_opFlag != opFlag::None)
				break;
			_showGameTitle();
			drawEntries();
		}
		_engine->updateWindow();
		while (true)
		{
//			if (std::chrono::duration_cast<std::chrono::milliseconds>(start.time_since_epoch()).count() <= TICK_INTERVAL)
			if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() <= DEFAULT_SPEED)
				continue;
			else
				break;
		}
	}
}

void Menu::keyboardEventHandler() {
	if (_opFlag == opFlag::ShowingText) {
		switch (_engine->keyBoardEvent()) {
			case eKey::Quit:
				endGame();
				break;
			case eKey::Enter:
				_opFlag = opFlag::None;
				break;
			case eKey::Sasuke:
				_opFlag = opFlag::None;
				break;
			case eKey::MoveLeft:
				_opFlag = opFlag::None;
				break ;
			case eKey::Pause:
				_opFlag = opFlag::None;
				break ;
			case eKey::Sdl:
				_funcForSwitchingEngines(eLib::Sdl2);
				break ;
			case eKey::Sfml:
				_funcForSwitchingEngines(eLib::Sfml);
				break ;
			case eKey::Opengl:
				_funcForSwitchingEngines(eLib::Opengl);
				break ;
			case eKey::None:
				break;
			default:
				break;
		}
		return ;
	}
	switch (_engine->keyBoardEvent()) {
		case eKey::Quit:
			endGame();
			break;
		case eKey::MoveUp:
			_selectUpperEntry();
			break;
		case eKey::MoveDown:
			_selectLowerEntry();
			break;
		case eKey::Enter:
			if (_curEntry->func) {
				_soundEngine->playSound(eSound::MenuSelect);
				_curEntry->func();
			}
			break;
		case eKey::Sasuke:
			if (_curEntry->func) {
				_soundEngine->playSound(eSound::MenuSelect);
				_curEntry->func();
			}
			break;
		case eKey::Pause:
			//TODO: opengl pause key bug
			startGame();
			break;
		case eKey::Sdl:
			_funcForSwitchingEngines(eLib::Sdl2);
			break ;
		case eKey::Sfml:
			_funcForSwitchingEngines(eLib::Sfml);
			break ;
		case eKey::Opengl:
			_funcForSwitchingEngines(eLib::Opengl);
			break ;
		case eKey::None:
			break;
		default:
			break;
	}
}

void Menu::setAllParameters(eColor mainColor, eColor addColor, eColor fontColor,
							eColor addFontColor, eFont Font, int FontSize) {
	_mainColor = mainColor;
	_addColor = addColor;
	_fontColor = fontColor;
	_addFontColor = addFontColor;
	_font = Font;
	_fontSize = FontSize;
}

void Menu::setEntryParameters(int entryWidth, int entryHeight) {
	_entryWidth = entryWidth;
	_entryHeight = entryHeight;
}

void Menu::addNewEntry(const std::string &text, int x, int y, bool selected, std::function<void()> func) {
	_menuEntries.emplace_back(text, y, x, selected, func);
}


void Menu::drawEntries() {
	int yOffset = 0;
	for (const auto & i : _menuEntries)
	{
		_engine->drawRectangle(i.selected ? _addColor : _mainColor, _xPos, _yPos + yOffset, _entryWidth, _entryHeight);
		_engine->drawText(i.selected ? _addFontColor :_fontColor, _font, _xPos + i.x, _yPos + yOffset + i.y, _fontSize, i.text);
		yOffset += (_entryHeight + MARGIN_BETWEEN_ENTRIES);
	}
}

void Menu::setPosition(int x, int y) {
	_xPos = x;
	_yPos = y;
}

void Menu::_selectLowerEntry() {
	if (_curEntry == std::prev(_menuEntries.end()))
		return ;
	_soundEngine->playSound(eSound::MenuChange);
	_curEntry->selected = false;
	(std::next(_curEntry))->selected = true;
	_curEntry++;
}

void Menu::_selectUpperEntry() {
	if (_curEntry == _menuEntries.begin())
		return ;
	_soundEngine->playSound(eSound::MenuChange);
	_curEntry->selected = false;
	(std::prev(_curEntry))->selected = true;
	_curEntry--;
}

void Menu::startGame() {
	_opFlag = opFlag::StartGame;
}

void Menu::startMulGame() {
	_opFlag = opFlag::StartMulGame;
}


void Menu::endGame() {
	_opFlag = opFlag::EndGame;
}

void Menu::_showGameTitle() {
	_engine->drawText(_fontColor, _font, _winWidth / 2 - 2, 1, TITLE_FONT_SIZE, _titleText);
}

#define SHIFT 10
void Menu::showText(const std::string & text) {
	_opFlag = opFlag::ShowingText;

	int yPosForText = 1;
	std::stringstream ss(text);
	std::string item;
	while (std::getline(ss, item, '\n'))
	{
		_engine->drawText(_fontColor, _font, _winWidth/2 - SHIFT, yPosForText++, TEXT_FONT_SIZE, item);
	}

	_engine->drawText(_addColor, _font, _winWidth/2 - SHIFT, _winHeight - 2, TEXT_FONT_SIZE, "Press left arrow/enter/space/escape to get to main menu");
}

void Menu::returnToMenu() {
	_opFlag = opFlag::MainMenu;
}

void Menu::setTitleText(const std::string &text) {
	_titleText = text;
}

void Menu::_deselectAllEntries() {
	for (auto & i : _menuEntries) {
		if (&i == &*_menuEntries.begin()) {
			i.selected = true;
			continue;
		}
		i.selected = false;
	}
}

void Menu::setSoundEngine(ISoundEngine *soundEngine) {
	_soundEngine = soundEngine;
}

void Menu::setFuncForDrawingBg(std::function<void()> func) {
	_funcForDrawingBg = std::move(func);
}

void Menu::setFuncForSwitchingEngines(std::function<void(eLib lib)> func) {
	_funcForSwitchingEngines = func;
}

Menu::MenuEntry::MenuEntry(const std::string &text, int y, int x, bool selected,
						std::function<void()> func) :
		text(text), x(x), y(y), selected(selected), func(func) {
}
