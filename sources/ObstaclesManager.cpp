/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ObstaclesManager.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 16:22:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/09/29 15:00:01 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <random>
#include "ObstaclesManager.h"

ObstaclesManager::ObstaclesManager() {
}

ObstaclesManager::~ObstaclesManager() {
}

void ObstaclesManager::setObjectsToCheckCollisionWith(const Snake *snake,
													  const FoodManager *foodManager) {
	_snake = snake;
	_foodManager = foodManager;
}

void ObstaclesManager::setWinAndHeight(int winWidth, int winHeight) {
	_winWidth = winWidth;
	_winHeight = winHeight;
}

void ObstaclesManager::setEngine(IEngine *engine) {
	_engine = engine;
}

void ObstaclesManager::spawnRandomlyObstacles(int numOfObstacles) {
	if (!obstacles.empty())
		obstacles.clear();
	std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<int> ranWidth(0, _winWidth - 1);
	std::uniform_int_distribution<int> ranHeight(0, _winHeight - 1);

	for (int i = 0; i < numOfObstacles; i++)
	{
		MapObject tmp;
		tmp.type = eObject::Obstacle;
		do {
			tmp.x = ranWidth(generator);
			tmp.y = ranHeight(generator);
		}
		while (!_checkCollision(tmp));
		obstacles.push_back(tmp);
	}
}

#define HEAD_PART (*_snake->parts.begin())
#define HEAD_PART2 (*_snake2->parts.begin())

bool ObstaclesManager::_checkCollision(const MapObject & obstacle) const {
	for (const auto & i : _snake->parts)
	{
		if (obstacle.x == i->x && obstacle.y == i->y)
			return false;
	}

	if (_snake2)
	{
		for (const auto & i : _snake2->parts)
		{
			if (obstacle.x == i->x && obstacle.y == i->y)
				return false;
		}
		if ((HEAD_PART2->x == obstacle.x && HEAD_PART2->y - 1 == obstacle.y)
			|| (HEAD_PART2->x == obstacle.x && HEAD_PART2->y - 2 == obstacle.y))
			return false;
	}
	//making sure obstacles don't spawn too close
	if ((HEAD_PART->x == obstacle.x && HEAD_PART->y - 1 == obstacle.y)
	|| (HEAD_PART->x == obstacle.x && HEAD_PART->y - 2 == obstacle.y))
		return false;


	if ((obstacle.x == _foodManager->getFood().x && obstacle.y == _foodManager->getFood().y)
	|| (obstacle.x == _foodManager->getSuperFood().x && obstacle.y == _foodManager->getSuperFood().y))
		return false;

	return true;
}

void ObstaclesManager::draw() {
	for (const auto & i : obstacles)
		_engine->drawSprite(eObject::Obstacle, eRotate::Up, i.x, i.y);
}

void ObstaclesManager::reset() {
	spawnRandomlyObstacles(obstacles.size());
}

void ObstaclesManager::setObjectsToCheckCollisionWith(const Snake *snake,
													  const Snake *snake2,
													  const FoodManager *foodManager) {
	_snake = snake;
	_snake2 = snake2;
	_foodManager = foodManager;
}

