#include "SfmlEngine.h"

void SfmlEngine::createWindow(int width, int height) {

	_window.create(sf::VideoMode(width * 2, height * 2), "Nibbler", sf::Style::Titlebar | sf::Style::Close);

    auto desktop = sf::VideoMode::getDesktopMode();
    _window.setPosition(sf::Vector2i(desktop.width * 0.5 - _window.getSize().x * 0.5,
                                      desktop.height * 0.5 - _window.getSize().y * 0.5));
}

void SfmlEngine::drawSprite(eObject objectType, eRotate rotate, int x, int y) {

	sf::Texture img;
	sf::Sprite sprite;

	if (!img.loadFromFile(ObjSpritesPath[static_cast<int>(objectType)].c_str())) {
		throw std::runtime_error(nullptr);
	}

	sprite.setTexture(img);
	sprite.setScale(2, 2);
	sprite.setPosition(x * 2.f * SPRITE_SIZE + SPRITE_SIZE, y * 2.f * SPRITE_SIZE + SPRITE_SIZE);
	sprite.setOrigin(SPRITE_SIZE / 2.f , SPRITE_SIZE / 2.f);

	switch (rotate) {
		case eRotate::Down:
			sprite.setRotation(180.f);
			break;
		case eRotate::Left:
			sprite.setRotation(270.f);
			break;
		case eRotate::Right:
			sprite.setRotation(90.f);
			break;
		default:
			break;
	}
	_window.draw(sprite);
}

void SfmlEngine::drawBackground(eColor color) {

	_window.clear(sf::Color(RgbColors[static_cast<int>(color)][0],
							RgbColors[static_cast<int>(color)][1],
							RgbColors[static_cast<int>(color)][2]));
}

void SfmlEngine::clearWindow() {

	_window.clear();
}

void SfmlEngine::updateWindow() {

	_window.display();
}

eKey SfmlEngine::keyBoardEvent() {

	sf::Event event;

	if (_window.pollEvent(event)) {
		switch (event.type) {
			case (sf::Event::Closed):
				return eKey::Quit;
			case (sf::Event::KeyPressed):
				switch (event.key.code) {
					case sf::Keyboard::Up:
						return eKey::MoveUp;
					case sf::Keyboard::Down:
						return eKey::MoveDown;
					case sf::Keyboard::Left:
						return eKey::MoveLeft;
					case sf::Keyboard::Right:
						return eKey::MoveRight;
					case sf::Keyboard::Q:
						return eKey::Quit;
					case sf::Keyboard::Return:
						return eKey::Enter;
					case sf::Keyboard::Escape:
						return eKey::Pause;
					case sf::Keyboard::Space:
						return eKey::Sasuke;
				    case sf::Keyboard::Num1:
                        return eKey::Sdl;
                    case sf::Keyboard::Num2:
                        return eKey::Sfml;
                    case sf::Keyboard::Num3:
                        return eKey::Opengl;
					default:
						break;
				}
				default:
					break;
		}
	}

	return eKey::None;
}

void SfmlEngine::drawText(eColor color, eFont font, int x, int y, int size, std::string text) {

	sf::Font textFont;
	if (!textFont.loadFromFile(Fonts[static_cast<int>(font)].c_str())) {
		throw std::runtime_error(nullptr);
	}

	sf::Text textString(text.c_str(), textFont, size * 2);
	textString.setPosition(x * 2 * SPRITE_SIZE, y * 2 * SPRITE_SIZE);
	textString.setFillColor(sf::Color(RgbColors[static_cast<int>(color)][0],
									  RgbColors[static_cast<int>(color)][1],
									  RgbColors[static_cast<int>(color)][2]));
	_window.draw(textString);
}

void SfmlEngine::drawRectangle(eColor color, int x, int y, int width, int height) {

	sf::RectangleShape rect(sf::Vector2f(width * 2.f * SPRITE_SIZE, height * 2.f * SPRITE_SIZE));

	rect.setFillColor(sf::Color(RgbColors[static_cast<int>(color)][0],
								RgbColors[static_cast<int>(color)][1],
								RgbColors[static_cast<int>(color)][2]));

	rect.setPosition(sf::Vector2f(x * 2 * SPRITE_SIZE, y * 2 * SPRITE_SIZE));

	_window.draw(rect);
}

bool SfmlEngine::keyBoardEvent(eKey key) {

    switch (key) {
//        case eKey::MoveUp:
//            return sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
//        case eKey::MoveDown:
//            return sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
//        case eKey::MoveLeft:
//            return sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
//        case eKey::MoveRight:
//            return sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
//        case eKey::Quit:
//            return sf::Keyboard::isKeyPressed(sf::Keyboard::Q);
//        case eKey::Enter:
//            return sf::Keyboard::isKeyPressed(sf::Keyboard::Return);
//        case eKey::Pause:
//            return sf::Keyboard::isKeyPressed(sf::Keyboard::Escape);
//        case eKey::Sasuke:
//            return sf::Keyboard::isKeyPressed(sf::Keyboard::Space);
        case eKey::W:
            return sf::Keyboard::isKeyPressed(sf::Keyboard::W);
        case eKey::S:
            return sf::Keyboard::isKeyPressed(sf::Keyboard::S);
        case eKey::A:
            return sf::Keyboard::isKeyPressed(sf::Keyboard::A);
        case eKey::D:
            return sf::Keyboard::isKeyPressed(sf::Keyboard::D);
        default:
            break;
    }
    return false;
}

extern "C" IEngine *create() {

	return new SfmlEngine();
}

extern "C" void destroy(IEngine *engine) {

	delete engine;
}
