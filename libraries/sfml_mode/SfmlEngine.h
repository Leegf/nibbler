#ifndef NIBBLER_SFMLENGINE_H
#define NIBBLER_SFMLENGINE_H

#include "IEngine.h"
#include <SFML/Graphics.hpp>

class SfmlEngine : public IEngine {

public:
    SfmlEngine() = default;
    ~SfmlEngine() = default;
    SfmlEngine(const SfmlEngine &) = delete;
    SfmlEngine &operator=(const SfmlEngine &) = delete;

	void createWindow(int width, int height) override;
	void clearWindow() override;
	void drawSprite(eObject objectType, eRotate rotate, int x, int y) override;
	void drawBackground(eColor color) override;
	void drawText(eColor color, eFont font, int x, int y, int size, std::string text) override;
	void drawRectangle(eColor color, int x, int y, int width, int height) override;
	void updateWindow() override;
	eKey keyBoardEvent() override;
    bool keyBoardEvent(eKey key) override;

private:
    sf::RenderWindow _window;
};


#endif //NIBBLER_SFMLENGINE_H
