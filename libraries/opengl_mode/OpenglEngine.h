#ifndef NIBBLER_OPENGLENGINE_H
#define NIBBLER_OPENGLENGINE_H

#define GL_SILENCE_DEPRECATION
#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "Config.h"
#include "IEngine.h"
#include "Shader.h"

class OpenglEngine : public IEngine {

public:
    OpenglEngine() = default;
	~OpenglEngine();
	OpenglEngine(const OpenglEngine &) = delete;
    OpenglEngine &operator=(const OpenglEngine &) = delete;

	void createWindow(int width, int height) override;
	void clearWindow() override;
	void drawSprite(eObject objectType, eRotate rotate, int x, int y) override;
	void drawBackground(eColor color) override;
	void drawText(eColor color, eFont font, int x, int y, int size, std::string text) override;
	void drawRectangle(eColor color, int x, int y, int width, int height) override;
	void updateWindow() override;
	eKey keyBoardEvent() override;
    bool keyBoardEvent(eKey key) override;

private:
	GLFWwindow *_window;

	Shader _spriteShader;
    Shader _textShader;
    Shader _rectangleShader;

    unsigned int _rectangleVAO;
    unsigned int _rectangleVBO;
    unsigned int _rectangleEBO;

    int _windowWidth;
    int _windowHeight;

	void init_all();
	void clear_all();
};


#endif //NIBBLER_OPENGLENGINE_H
