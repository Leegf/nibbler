#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>


class Shader
{
public:
    unsigned int _program;

    Shader() = default;
    ~Shader();
    Shader(const Shader &) = delete;
    Shader &operator=(const Shader &) = delete;

    void init(const GLchar* vertexPath, const GLchar* fragmentPath);
    void use();
    void setVec3(const std::string &name, float value1, float value2, float value3) const;

private:
    void checkCompileErrors(unsigned int shader, std::string type);
};

#endif