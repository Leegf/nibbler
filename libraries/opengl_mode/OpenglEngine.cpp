#include "OpenglEngine.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <map>
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"
#include <thread>
#include <zconf.h>

void OpenglEngine::createWindow(int width, int height) {

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	_window = glfwCreateWindow(width, height, "Nibbler", nullptr, nullptr);
    const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    glfwSetWindowPos(_window, (mode->width - width) * 0.5, (mode->height - height) * 0.5);

	if (_window == nullptr) {
		glfwTerminate();
		throw std::runtime_error("Error occured in OpenGL: Failed to create GLFW _window");
	}
	glfwMakeContextCurrent(_window);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		throw std::runtime_error("Failed to initialize GLEW");
	}

	glViewport(0, 0, width, height);
    init_all();
}

void OpenglEngine::clearWindow() {

	glClear(GL_COLOR_BUFFER_BIT);
}

void OpenglEngine::init_all() {

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glfwGetWindowSize(_window, &_windowWidth, &_windowHeight);
    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(_windowWidth), 0.0f, static_cast<GLfloat>(_windowHeight));

    // Init drawSprite
    _spriteShader.init("libraries/opengl_mode/shaders/texture.vs", "libraries/opengl_mode/shaders/texture.frag");
    _spriteShader.use();
    glUniformMatrix4fv(glGetUniformLocation(_spriteShader._program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    // Init drawText
    _textShader.init("libraries/opengl_mode/shaders/text.vs", "libraries/opengl_mode/shaders/text.frag");
    _textShader.use();
    glUniformMatrix4fv(glGetUniformLocation(_textShader._program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    // Init drawRectangle
    _rectangleShader.init("libraries/opengl_mode/shaders/rectangle.vs", "libraries/opengl_mode/shaders/rectangle.frag");
    _rectangleShader.use();
    glUniformMatrix4fv(glGetUniformLocation(_rectangleShader._program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    unsigned int indices[] = {
            0, 1, 3,
            1, 2, 3
    };
    glGenVertexArrays(1, &_rectangleVAO);
    glGenBuffers(1, &_rectangleVBO);
    glGenBuffers(1, &_rectangleEBO);
    glBindVertexArray(_rectangleVAO);
    glBindBuffer(GL_ARRAY_BUFFER, _rectangleVBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _rectangleEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

}

void OpenglEngine::drawSprite(eObject objectType, eRotate rotate, int x, int y) {

    x *= SPRITE_SIZE;
    y *= SPRITE_SIZE;

    float vertices[] = {
            static_cast<float>(x + SPRITE_SIZE), static_cast<float>(_windowHeight - y), 0.993f, 0.993f,
            static_cast<float>(x + SPRITE_SIZE), static_cast<float>(_windowHeight - y - SPRITE_SIZE), 0.993f, 0.007f,
            static_cast<float>(x), static_cast<float>(_windowHeight - y - SPRITE_SIZE), 0.007f, 0.007f,
            static_cast<float>(x),  static_cast<float>(_windowHeight - y), 0.007f, 0.993f
    };

    switch (rotate) {
        case eRotate::Left:
            vertices[2] = 0.993f;
            vertices[3] = 0.007f;
            vertices[6] = 0.007f;
            vertices[7] = 0.007f;
            vertices[10] = 0.007f;
            vertices[11] = 0.993f;
            vertices[14] = 0.993f;
            vertices[15] = 0.993f;
            break;
        case eRotate::Down:
            vertices[2] = 0.007f;
            vertices[3] = 0.007f;
            vertices[6] = 0.007f;
            vertices[7] = 0.993f;
            vertices[10] = 0.993f;
            vertices[11] = 0.993f;
            vertices[14] = 0.993f;
            vertices[15] = 0.007f;
            break;
        case eRotate::Right:
            vertices[2] = 0.007f;
            vertices[3] = 0.993f;
            vertices[6] = 0.993f;
            vertices[7] = 0.993f;
            vertices[10] = 0.993f;
            vertices[11] = 0.007f;
            vertices[14] = 0.007f;
            vertices[15] = 0.007f;
            break;
        default:
            break;
    }

    unsigned int indices[] = {
            0, 1, 3,
            1, 2, 3
    };
    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load(ObjSpritesPath[static_cast<int>(objectType)].c_str(), &width, &height, &nrChannels, 0);
    if (data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else {
        throw std::runtime_error("Error occured in OpenGL: Failed to loat texture");
    }
    stbi_image_free(data);

    glBindTexture(GL_TEXTURE_2D, texture);

    _spriteShader.use();
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    glDeleteTextures(1, &texture);
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
}

void OpenglEngine::drawBackground(eColor color) {

	glClearColor(RgbColors[static_cast<int>(color)][0] / 255.0f,
				 RgbColors[static_cast<int>(color)][1] / 255.0f,
				 RgbColors[static_cast<int>(color)][2] / 255.0f, 1.0f);
}

void OpenglEngine::drawText(eColor color, eFont font, int x, int y, int size, std::string text) {

    x *= SPRITE_SIZE;
    y *= SPRITE_SIZE;

    struct Character {
        GLuint TextureID;
        glm::ivec2 Size;
        glm::ivec2 Bearing;
        GLuint Advance;
    };
    std::map<GLchar, Character> Characters;
    GLuint VAO1, VBO;

    FT_Library ft;
    if (FT_Init_FreeType(&ft)) {
        throw std::runtime_error("Error occured in OpenGL: Failed to init FreeType Library");
    }

    FT_Face face;
    if (FT_New_Face(ft, Fonts[static_cast<int>(font)].c_str(), 0, &face)) {
        throw std::runtime_error("Error occured in OpenGL: Failed to load font");
    }

    FT_Set_Pixel_Sizes(face, 0, size);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    for (GLubyte c = 0; c < 128; c++) {
        if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
            throw std::runtime_error("Error occured in OpenGL: Failed to load Glyph");
        }
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RED,
                face->glyph->bitmap.width,
                face->glyph->bitmap.rows,
                0,
                GL_RED,
                GL_UNSIGNED_BYTE,
                face->glyph->bitmap.buffer
        );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        Character character = {
                texture,
                glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                static_cast<GLuint>(face->glyph->advance.x)
        };
        Characters.insert(std::pair<GLchar, Character>(c, character));
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    FT_Done_Face(face);
    FT_Done_FreeType(ft);

    glGenVertexArrays(1, &VAO1);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO1);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    _textShader.use();
    glUniform3f(glGetUniformLocation(_textShader._program, "textColor"), RgbColors[static_cast<int>(color)][0] / 255.f,
                                                                       RgbColors[static_cast<int>(color)][1] / 255.f,
                                                                       RgbColors[static_cast<int>(color)][2] / 255.f);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(VAO1);

    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++) {
        Character ch = Characters[*c];

        GLfloat xpos = x + ch.Bearing.x;
        GLfloat ypos = _windowHeight - y - (ch.Size.y + ch.Bearing.y) + 26;

        GLfloat w = ch.Size.x;
        GLfloat h = ch.Size.y;
        GLfloat vertices[6][4] = {
                { xpos,     ypos + h,   0.0, 0.0 },
                { xpos,     ypos,       0.0, 1.0 },
                { xpos + w, ypos,       1.0, 1.0 },

                { xpos,     ypos + h,   0.0, 0.0 },
                { xpos + w, ypos,       1.0, 1.0 },
                { xpos + w, ypos + h,   1.0, 0.0 }
        };
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        x += (ch.Advance >> 6) * 1;
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);

    for (GLubyte i = 0; i < 128; i++) {
        glDeleteTextures(1, &Characters[i].TextureID);
    }

    glDeleteVertexArrays(1, &VAO1);
    glDeleteBuffers(1, &VBO);
}

void OpenglEngine::drawRectangle(eColor color, int x, int y, int width, int height) {

    x *= SPRITE_SIZE;
    y *= SPRITE_SIZE;
    width *= SPRITE_SIZE;
    height *= SPRITE_SIZE;

    float vertices[] = {
            static_cast<float>(x + width),  static_cast<float>(_windowHeight - y), 0.0f,
            static_cast<float>(x + width), static_cast<float>(_windowHeight - y - height), 0.0f,
            static_cast<float>(x), static_cast<float>(_windowHeight - y - height), 0.0f,
            static_cast<float>(x),  static_cast<float>(_windowHeight - y), 0.0f
    };

    glBindBuffer(GL_ARRAY_BUFFER, _rectangleVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
//    glBindBuffer(GL_ARRAY_BUFFER, 0);

    _rectangleShader.use();
    _rectangleShader.setVec3("color", RgbColors[static_cast<int>(color)][0] / 255.f,
                                     RgbColors[static_cast<int>(color)][1] / 255.f,
                                     RgbColors[static_cast<int>(color)][2] / 255.f);
    glBindVertexArray(_rectangleVAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
//    glBindVertexArray(0);
}

void OpenglEngine::updateWindow() {

	glfwSwapBuffers(_window);
}

eKey OpenglEngine::keyBoardEvent() {

	glfwPollEvents();

	static bool escapePressed = false;
    static bool spacePressed = false;

    if (glfwGetKey(_window, GLFW_KEY_Q) == GLFW_PRESS || glfwWindowShouldClose(_window)) {
		return eKey::Quit;
	}
	else if (glfwGetKey(_window, GLFW_KEY_UP) == GLFW_PRESS) {
		return eKey::MoveUp;
	}
	else if (glfwGetKey(_window, GLFW_KEY_DOWN) == GLFW_PRESS) {
		return eKey::MoveDown;
	}
	else if (glfwGetKey(_window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		return eKey::MoveLeft;
	}
	else if (glfwGetKey(_window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		return eKey::MoveRight;
	}
	else if (glfwGetKey(_window, GLFW_KEY_ENTER) == GLFW_PRESS) {
		return eKey::Enter;
	}
	else if (glfwGetKey(_window, GLFW_KEY_SPACE) == GLFW_RELEASE && spacePressed) {
	    spacePressed = false;
		return eKey::None;
	}
    else if (glfwGetKey(_window, GLFW_KEY_SPACE) == GLFW_PRESS && !spacePressed) {
        spacePressed = true;
        return eKey::Sasuke;
    }
    else if (glfwGetKey(_window, GLFW_KEY_ESCAPE) == GLFW_RELEASE && escapePressed) {
        escapePressed = false;
        return eKey::None;
    }
	else if (glfwGetKey(_window, GLFW_KEY_ESCAPE) == GLFW_PRESS && !escapePressed) {
	    escapePressed = true;
		return eKey::Pause;
	}
    else if (glfwGetKey(_window, GLFW_KEY_1) == GLFW_PRESS) {
        return eKey::Sdl;
    }
    else if (glfwGetKey(_window, GLFW_KEY_2) == GLFW_PRESS) {
        return eKey::Sfml;
    }
    else if (glfwGetKey(_window, GLFW_KEY_3) == GLFW_PRESS) {
        return eKey::Opengl;
    }

	return eKey::None;
}

OpenglEngine::~OpenglEngine() {

    clear_all();
	glfwTerminate();
}

bool OpenglEngine::keyBoardEvent(eKey key) {

    glfwPollEvents();

    switch (key) {
//        case eKey::MoveUp:
//            return glfwGetKey(_window, GLFW_KEY_UP) == GLFW_PRESS;
//        case eKey::MoveDown:
//            return glfwGetKey(_window, GLFW_KEY_DOWN) == GLFW_PRESS;
//        case eKey::MoveLeft:
//            return glfwGetKey(_window, GLFW_KEY_LEFT) == GLFW_PRESS;
//        case eKey::MoveRight:
//            return glfwGetKey(_window, GLFW_KEY_RIGHT) == GLFW_PRESS;
//        case eKey::Enter:
//            return glfwGetKey(_window, GLFW_KEY_ENTER) == GLFW_PRESS;
//        case eKey::Pause:
//            return glfwGetKey(_window, GLFW_KEY_ESCAPE) == GLFW_PRESS;
//        case eKey::Sasuke:
//            return glfwGetKey(_window, GLFW_KEY_SPACE) == GLFW_PRESS;
        case eKey::W:
            return glfwGetKey(_window, GLFW_KEY_W) == GLFW_PRESS;
        case eKey::A:
            return glfwGetKey(_window, GLFW_KEY_A) == GLFW_PRESS;
        case eKey::S:
            return glfwGetKey(_window, GLFW_KEY_S) == GLFW_PRESS;
        case eKey::D:
            return glfwGetKey(_window, GLFW_KEY_D) == GLFW_PRESS;
        case eKey::Quit:
            return glfwGetKey(_window, GLFW_KEY_Q) == GLFW_PRESS;
        case eKey::None:
            return false;
        default:
            return false;
    }
    return false;
}

void OpenglEngine::clear_all() {

    // clear rectangle
    glDeleteBuffers(1, &_rectangleVBO);
    glDeleteBuffers(1, &_rectangleEBO);
    glDeleteVertexArrays(1, &_rectangleVAO);
}

extern "C" IEngine *create() {

	return new OpenglEngine();
}

extern "C" void destroy(IEngine *engine) {

	delete engine;
}
