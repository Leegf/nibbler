#version 140

in vec3 aPos;
uniform mat4 projection;

void main() {
    gl_Position = projection * vec4(aPos.x, aPos.y, aPos.z, 1.0);
}
