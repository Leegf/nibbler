#ifndef NIBBLER_SDLENGINE_H
#define NIBBLER_SDLENGINE_H

#include <SDL2/SDL.h>
#include "IEngine.h"
#include <memory>

using sdl_texture = std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)>;
using sdl_surface = std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>;
using sdl_font = std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)>;
using sdl_window = std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>;
using sdl_renderer = std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)>;

class SdlEngine : public IEngine {

public:
	SdlEngine();
	~SdlEngine();
	SdlEngine(const SdlEngine &) = delete;
	SdlEngine &operator=(const SdlEngine &) = delete;

	void createWindow(int width, int height) override;
	void clearWindow() override;
	void drawSprite(eObject objectType, eRotate rotate, int x, int y) override;
	void drawBackground(eColor color) override;
	void drawText(eColor color, eFont font, int x, int y, int size, std::string text) override;
	void drawRectangle(eColor color, int x, int y, int width, int height) override;
	void updateWindow() override;
	eKey keyBoardEvent() override;
    bool keyBoardEvent(eKey key) override;

private:
	sdl_window _window;
	sdl_renderer _windowRenderer;
};

#endif //NIBBLER_SDLENGINE_H
