#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <zconf.h>
#include "SdlEngine.h"

SdlEngine::SdlEngine() :
	_window(nullptr, SDL_DestroyWindow),
	_windowRenderer(nullptr, SDL_DestroyRenderer) {

}

void SdlEngine::createWindow(int width, int height) {

    SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);

	if(TTF_Init() < 0) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + TTF_GetError());
	}

	_window.reset(SDL_CreateWindow("Nibbler",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_INPUT_FOCUS));
	if (_window == nullptr) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}

	_windowRenderer.reset(SDL_CreateRenderer(_window.get(), -1, SDL_RENDERER_ACCELERATED));
	if (_windowRenderer == nullptr) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}
}

void SdlEngine::clearWindow() {

	if (SDL_RenderClear(_windowRenderer.get()) < 0) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}
}

void SdlEngine::drawSprite(eObject objectType, eRotate rotate, int x, int y) {

	sdl_texture img(IMG_LoadTexture(_windowRenderer.get(),
			ObjSpritesPath[static_cast<int>(objectType)].c_str()), SDL_DestroyTexture);

	if (img == nullptr) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}

	SDL_Rect sprite;
	if (SDL_QueryTexture(img.get(), nullptr, nullptr, &sprite.w, &sprite.h) < 0) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}
	sprite.x = x * SPRITE_SIZE;
	sprite.y = y * SPRITE_SIZE;

	int ret;
	switch (rotate) {
		case eRotate::Up:
			ret = SDL_RenderCopyEx(_windowRenderer.get(), img.get(), nullptr, &sprite, 0, nullptr, SDL_FLIP_NONE);
			break;
		case eRotate::Down:
			ret = SDL_RenderCopyEx(_windowRenderer.get(), img.get(), nullptr, &sprite, 180, nullptr, SDL_FLIP_NONE);
			break;
		case eRotate::Left:
			ret = SDL_RenderCopyEx(_windowRenderer.get(), img.get(), nullptr, &sprite, 270, nullptr, SDL_FLIP_NONE);
			break;
		case eRotate::Right:
			ret = SDL_RenderCopyEx(_windowRenderer.get(), img.get(), nullptr, &sprite, 90, nullptr, SDL_FLIP_NONE);
			break;
		default:
			break;
	}
	if (ret < 0) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}

	SDL_DestroyTexture(img.get());
}

void SdlEngine::updateWindow() {

	SDL_RenderPresent(_windowRenderer.get());
}

eKey SdlEngine::keyBoardEvent() {

	SDL_Event event;

	if (SDL_PollEvent(&event)) {
		switch (event.type) {
			case SDL_QUIT:
				return eKey::Quit;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
					case SDLK_UP:
						return eKey::MoveUp;
					case SDLK_DOWN:
						return eKey::MoveDown;
					case SDLK_LEFT:
						return eKey::MoveLeft;
					case SDLK_RIGHT:
						return eKey::MoveRight;
					case SDLK_q:
						return eKey::Quit;
					case SDLK_RETURN:
					case SDLK_KP_ENTER:
						return eKey::Enter;
					case SDLK_ESCAPE:
						return eKey::Pause;
					case SDLK_SPACE:
						return eKey::Sasuke;
                    case SDLK_1:
                        return eKey::Sdl;
                    case SDLK_2:
                        return eKey::Sfml;
                    case SDLK_3:
                        return eKey::Opengl;
					default:
                        return eKey::None;
				}
			default:
                return eKey::None;
		}
	}

	return eKey::None;
}

SdlEngine::~SdlEngine() {

	IMG_Quit();
	TTF_Quit();
}

void SdlEngine::drawBackground(eColor color) {

	SDL_SetRenderDrawColor(_windowRenderer.get(), RgbColors[static_cast<int>(color)][0],
			RgbColors[static_cast<int>(color)][1], RgbColors[static_cast<int>(color)][2], SDL_ALPHA_OPAQUE);

	if (SDL_RenderClear(_windowRenderer.get()) < 0) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}
}

void SdlEngine::drawText(eColor color, eFont font, int x, int y, int size, std::string text) {

	sdl_font text_font(TTF_OpenFont(Fonts[static_cast<int>(font)].c_str(), size), TTF_CloseFont);

	if (!text_font.get()) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + TTF_GetError());
	}

	SDL_Color text_color = { RgbColors[static_cast<int>(color)][0],
							 RgbColors[static_cast<int>(color)][1],
							 RgbColors[static_cast<int>(color)][2], 0 };

    sdl_surface surface(TTF_RenderText_Solid(text_font.get(), text.c_str(), text_color), SDL_FreeSurface);
	if (!surface.get()) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + TTF_GetError());
	}

	sdl_texture texture(SDL_CreateTextureFromSurface(_windowRenderer.get(), surface.get()), SDL_DestroyTexture);

	if (!texture.get()) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + TTF_GetError());
	}

	int width = 0;
	int height = 0;
	SDL_QueryTexture(texture.get(), NULL, NULL, &width, &height);

	SDL_Rect rect = { x * SPRITE_SIZE, y * SPRITE_SIZE + 3, width, height };

	SDL_RenderCopy(_windowRenderer.get(), texture.get(), NULL, &rect);
}

void SdlEngine::drawRectangle(eColor color, int x, int y, int width, int height) {

	SDL_Rect rect;
	rect.x = x * SPRITE_SIZE;
	rect.y = y * SPRITE_SIZE;
	rect.w = width * SPRITE_SIZE;
	rect.h = height * SPRITE_SIZE;

	if (SDL_SetRenderDrawColor(_windowRenderer.get(), RgbColors[static_cast<int>(color)][0],
		RgbColors[static_cast<int>(color)][1], RgbColors[static_cast<int>(color)][2], SDL_ALPHA_OPAQUE) < 0) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}

	if (SDL_RenderFillRect(_windowRenderer.get(), &rect) < 0) {
		throw std::runtime_error(std::string("Error occurred in SDL2: ") + SDL_GetError());
	}
}

bool SdlEngine::keyBoardEvent(eKey key) {

    const Uint8 *keys = SDL_GetKeyboardState(NULL);
    SDL_Event event;
    SDL_PollEvent(&event);

    switch (key) {
//        case eKey::MoveUp:
//            return keys[SDL_SCANCODE_UP];
//        case eKey::MoveDown:
//            return keys[SDL_SCANCODE_DOWN];
//        case eKey::MoveLeft:
//            return keys[SDL_SCANCODE_LEFT];
//        case eKey::MoveRight:
//            return keys[SDL_SCANCODE_RIGHT];
//        case eKey::Quit:
//            return keys[SDL_SCANCODE_Q];
//        case eKey::Enter:
//            return keys[SDL_SCANCODE_KP_ENTER];
//        case eKey::Pause:
//            return keys[SDL_SCANCODE_ESCAPE];
//        case eKey::Sasuke:
//            return keys[SDL_SCANCODE_SPACE];
        case eKey::W:
            return keys[SDL_SCANCODE_W];
        case eKey::A:
            return keys[SDL_SCANCODE_A];
        case eKey::S:
            return keys[SDL_SCANCODE_S];
        case eKey::D:
            return keys[SDL_SCANCODE_D];
        case eKey::None:
            return false;
        default:
            return false;
    }

    return false;
}

extern "C" IEngine *create() {

	return new SdlEngine();
}

extern "C" void destroy(IEngine *engine) {

	delete engine;
}
