#ifndef NIBBLER_SOUNDENGINE_H
#define NIBBLER_SOUNDENGINE_H

#include <SDL2/SDL_mixer.h>
#include "ISoundEngine.h"
#include "Config.h"

class SoundEngine : public ISoundEngine {

public:
	SoundEngine();
	~SoundEngine();
	void playSound(eSound sound);
	void playMusic(eMusic music);

private:
	Mix_Chunk *_soundEffects[SOUNDS_NUM];
	Mix_Music *_musicTracks[MUSIC_NUM];
};


#endif //NIBBLER_SOUNDENGINE_H
