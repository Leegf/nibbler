#include "SoundEngine.h"
#include <SDL2/SDL.h>

SoundEngine::SoundEngine() {

	if (SDL_Init(SDL_INIT_AUDIO) != 0) {
		throw std::runtime_error(std::string("Error occurred in SDL MIXER: ") + SDL_GetError());
	}

	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096 ) < 0) {
		throw std::runtime_error(std::string("Error occurred in SDL MIXER: ") + Mix_GetError());
	}

	for (int i = 0; i < SOUNDS_NUM; i++) {
		_soundEffects[i] = Mix_LoadWAV(Sounds[i].c_str());
		if (_soundEffects[i] == NULL) {
			throw std::runtime_error(std::string("Error occurred in SDL MIXER: ") + Mix_GetError());
		}
	}
	for (int i = 0; i < MUSIC_NUM; i++) {
		_musicTracks[i] = Mix_LoadMUS(Music[i].c_str());
		if (_musicTracks[i] == NULL) {
			throw std::runtime_error(std::string("Error occurred in SDL MIXER: ") + Mix_GetError());
		}
	}
}

void SoundEngine::playSound(eSound sound) {

	if (Mix_PlayChannel(-1, _soundEffects[static_cast<int>(sound)], 0) == -1) {
		throw std::runtime_error(std::string("Error occurred in SDL MIXER: ") + Mix_GetError());
	}
}

SoundEngine::~SoundEngine() {

	for (int i = 0; i < SOUNDS_NUM; i++) {
		Mix_FreeChunk(_soundEffects[i]);
	}
	for (int i = 0; i < MUSIC_NUM; i++) {
		Mix_FreeMusic(_musicTracks[i]);
	}

	Mix_CloseAudio();
	SDL_Quit();
}

void SoundEngine::playMusic(eMusic music) {

	if (Mix_PlayMusic(_musicTracks[static_cast<int>(music)], -1) == -1) {
		throw std::runtime_error(std::string("Error occurred in SDL MIXER: ") + Mix_GetError());
	}
}

extern "C" ISoundEngine *create() {

	return new SoundEngine();
}

extern "C" void destroy(ISoundEngine *engine) {

	delete engine;
}
